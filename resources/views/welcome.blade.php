@extends('layouts.master')
@section('title', 'Setor Tugas Laravel Sanbercode')
@section('content')
<table class="table table-bordered">
    <thead>
        <tr>
            <th style="width: 10px">No.</th>
            <th>Nama Tugas</th>
            <th>Progress Tugas</th>
            <th style="width: 40px">Label</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.</td>
            <td><a href="/table" target="_blank">Table Page</a></td>
            <td>
                <div class="progress progress-xs">
                    <div
                        class="progress-bar progress-bar-danger"
                        style="width: 100%"
                    ></div>
                </div>
            </td>
            <td><span class="badge bg-danger">100%</span></td>
        </tr>
        <tr>
            <td>2.</td>
            <td><a href="/data-table" target="_blank">Data Table Page</a></td>
            <td>
                <div class="progress progress-xs">
                    <div
                        class="progress-bar bg-warning"
                        style="width: 100%"
                    ></div>
                </div>
            </td>
            <td><span class="badge bg-warning">100%</span></td>
        </tr>
    </tbody>
</table>
@endsection